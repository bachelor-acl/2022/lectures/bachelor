# Git da gitt

## Team

Nicholas Bodvis Sellevåg <br>
| nicholbs@stud.ntnu.no

Oddbjørn Sivert Borge-Jensen <br>
| oddbjosb@stud.ntnu.no

Fabian Kongelf <br>
| fabiako@stud.ntnu.no 

Yan Senko <br>
| yans@stud.ntnu.no

## Front matter

Added to the top of a markdown post to set certain specifications. Most of the typical content of the front matter is either set automatically or have default values, but in case you should want to change anything manually, here are the defaults and syntax:

>\-\-\-  
>layout: post  
>toc: true  
>\-\-\-  

### Order of content

Left of the NTNU logo are two links. They reffer to the posts order. The left-most link depicts the priveous post.
If you are not on a post it will link to the last post. In the case you are currently viewing the third post, it will link to its priveous post, namely the second.

The right-most link follows the same convention. If a user is not currently viewing a post, it points to the first ordered post. If the user is viewing a post, it will refer to the next post in order. For example viewing the third post will direct users to the fourth. 

In practice, in the Front matter of a post (beginning of a markdown document) include the following to specify that a post is the first:
```
order: 0
```

Consequently, to make another page the next in order add:
```
order: 1
```

## Layout

Defines the layout to use for a post. For now all posts should use the standard layout "post", as seen in *_layouts/post.html*.
In practice, layouts are templates with some basic html that includes the content of a given page, in addition a standarized footer and header all available inthe "/webpage/_includes" folder. 

## Table of Contents

Posts will automatically generate a table of contents based on headings present in the source markdown file. Tocs are enabled by default, but can be disabled for a single post by adding the following line to the front matter of the post:

### Example
>\-\-\-  
>toc: false  
>\-\-\-  

## Syntax highlighting

To render a code block with syntax highlighting, surround your code as follows:

>{% highlight c %}  
>  
>#include <stdio.h>  
>  
>int main() {  
>&ensp;&ensp;printf("Hello, World!");  
>&ensp;&ensp;return 0;  
>}  
>  
>{% endhighlight %}   

The argument to the highlight tag (C in the example above) is the language identifier. You can view a list of all supported languages and their identifiers [here](https://github.com/rouge-ruby/rouge/blob/master/docs/Languages.md).

## Naming convention

Markdown files to become websites needs to be named ".md" as of now because of "datoBeGone" plugin as it only looks for ".md" extension.
TODO: make markdown extension all encompassing

## Directory/folder structure

All documents to become websites and relating images should be created inside the "content" folder in its own subfolder. This subfolder will automatically be made into a category by Jekyll.

For example:

```
content/
    myFolder/
        -> markdown.md
        -> image.png
        -> document.pdf
    anotherFolder/
        -> markdownToBeWebsite2.md
        -> imageToWebsite2.png
        -> imageToWebsite2.pdf
```

This is not in and of itself a valid Jekyll structure, but script.sh will take the contents of the "content" folder and copy it into a temp folder "webpage/content", rearranging into a proper structure in the process. Should you as a user want to define the entire structure manually, simply adding the required _posts folders to the original "content" folder will stop the script from making any changes. 

```
content/
    myFolder/
        _posts/
            -> markdown.md
            -> image.png
            -> document.pdf
    anotherFolder/
        _posts/
            -> markdownToBeWebsite2.md
            -> imageToWebsite2.png
            -> imageToWebsite2.pdf
```

As long as an image or pdf is in the same folder as a markdown file, you can refer to them directly:

```
![My image](image.png)
```

If you want one or more assets to be accessed by several markdown files in several different folders, we recommend adding it to the "assets" folder. Referencing files from this folder requires using the absolute path starting at the baseurl:

```
![My image](/bachelor/webpage/assets/image.png)
```

## Notes for ourselves

### Liquid expressions
Link to jekyll-liquid documentation: https://jekyllrb.com/docs/liquid/
Link to liqud documentation: https://shopify.github.io/liquid/

Jekyll uses the Liquid templating language to process templates.

Generally in Liquid you output content using two curly braces e.g. `{{ variable }}` and perform logic statements by surrounding them in a curly brace percentage sign e.g. `{% if statement %}`.

In practice, we have used liquid to dynamically create the content listing on our index/home page. It serves the purpose of mapping our folder structure into the webpage and have links to all posts created for the site.

### Syntax highlighting

To change individual styles, for example colorschemes on variables and functions, change the css inside:
_syntax-highlighting.scss

Changing color for a specific language would probably involve:
Make a code snippet, highlight in browser which class names the ruby identifier used in the converted html
change the class css in _syntax-highlighting.scss.

For example, changing color of functions:
.n { color ....}    //n stands for name
.nf { color ...}    //nf stands for named function

NB! _base.scss has some base css for code highlighting. for example background color

### Performance testing

Link: https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html

NB! Check whether NTNU uses kubernetes cluster or not for Gitlab, metrics attained from website will depend on setup

### Plugins

DO NOT REMOVE THE _plugins FOLDER! It might be empty, but plugins won't work without it!


