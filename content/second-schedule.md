---
order: 0
---

# About the Course

## Expected background (from fall courses)

* Linux command line
* Basics of computers, network and virtual machines (VMs)
* Some programming skills
* Some security mindset

## Course setup

| Week | Day, Time, Location  |
| ---- | ------------- |
| 2 | Tue 16-18 (Digital, Gjøvik, Erik), Thu 14-16 (Digital, Gjøvik, Erik) |
| 3 | Mon 8-10 (Digital, Felles, Tor Ivar), Wed 11-12 (Digital, Felles, Erik), Thu 14-16 (Digital, Felles, Erik) |
| 4 | Mon 8-10 (Digital, Felles, Erik), Wed 11-13 (S206) |
| 5 | Tue 16-18 (2/3 Eureka), Wed 11-13 (S206) |
| 6 | Tue 16-18 (2/3 Eureka), Thu 14-16 (2/3 Eureka) |
| 7 | Tue 16-18 (2/3 Eureka), Thu 14-16 (2/3 Eureka) |
| 8 | Tue 16-18 (2/3 Eureka), Wed 11-13 (S206) |
| 9 | Tue 16-18 (2/3 Eureka), Thu 14-16 (S206) |
| 10 | Tue 16-18 (2/3 Eureka), Wed 11-13 (S206) |
| 11 | Tue 16-18 (2/3 Eureka), Wed 11-13 (S206) |
| 12 | Tue 16-18 (2/3 Eureka), Wed 11-13 (S206) |
| 13 | Påskeferie |
| 14 | Tue 16-18 (2/3 Eureka), Wed 11-13 (S206) |
| 15 | Tue 16-18 (2/3 Eureka), Wed 11-13 (S206) |
| 16 | Tue 16-18 (2/3 Eureka), Wed 11-13 (S206) |
| 17 | Tue 16-18 (2/3 Eureka), Wed 11-13 (S206) |

[See TP](https://tp.uio.no/ntnu/timeplan/?id=DCSG1005&type=course&sort=form&week=2&weekTo=17&ar=2021) for official schedule. **Note: We expect frequent changes as we sometimes merge teaching between campuses, check schedule above in case TP is not updated**

Teaching assistants are available every week, see contact info in Blackboard.

All sessions with teacher are [available in
Omnom](https://forelesning.gjovik.ntnu.no/publish/index.php?lecturer=Erik+Hjelm%C3%A5s&topic=all)
and/or [in
Stream](https://web.microsoftstream.com/group/c0195c45-6aa3-4b2b-9e19-3ef54402b9e0) (for recordings of joint sessions with Trondheim, see [the other Stream group](https://web.microsoftstream.com/group/3673fa2d-8c94-4b1f-96e4-83ee33f37595)).

## Readings

* See links in "Topic and Readings" in the [Weekly Schedule](https://gitlab.com/erikhje/dcsg1005/-/blob/master/schedule.md#weekly-schedule).
* Compendia with all lecture notes, review questions, problems and lab exercises
  ([PDF](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf) for
  reading,
  [Markdown](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.md) for
  easy copy-and-paste).

## Portfolio assessment

* Each portfolio assignment are announced on Monday with hand-in on Sunday two
  weeks later (meaning you have three weeks on each assignment and your workload
  should be roughly 25 hours)
* A portfolio assignment should be handed in as a git-repository with code and a
  report written in Markdown
* From the course description: "Mappevurdering gir grunnlag for sluttkarakteren
  i emnet. Mappen består av fire oppgaver hvorav sluttkarakter settes basert på
  de tre beste oppgavene. Alle oppgavene teller likt. Hver oppgave gis en
  midlertidig bokstavkarakter med begrunnelse. Sluttkarakter beregnes ut fra en
  helhetlig vurdering av de tre beste oppgavene. Av de fire oppgavene er det tre
  individuelle innleveringer og en gruppeinnlevering. Studenten kan velge å
  reinnlevere en av de tre individuelle innleveringene for ny vurdering. En slik
  ny vurdering kan kun forbedre eller forverre opprinnelig karakter med ett
  karaktertrinn."

## Weekly schedule

| Week Number and Day |  Topic and Readings | Exercises/Portfolio |
| ---  | ---                 | ---                 |
| 2 | **Introduction and tools** ([Compendia Chp 1](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf), [Introduksjon Windows Server](https://ntnu.blackboard.com/bbcswebdav/pid-1230441-dt-content-rid-33368006_1/xid-33368006_1), [Introduksjon PowerShell](https://ntnu.blackboard.com/bbcswebdav/pid-1230443-dt-content-rid-33368007_1/xid-33368007_1), [OpenStack](https://en.wikipedia.org/w/index.php?title=OpenStack&oldid=991678601), [Visual Studio Code](https://code.visualstudio.com/docs/getstarted/introvideos), [git](https://about.gitlab.com/images/press/git-cheat-sheet.pdf), [Markdown](https://docs.gitlab.com/ee/user/markdown.html))<br/>OpenStack GUI, ssh, git, vscode, powershell, public/private keys, key-based authentication, Heat-stack with OpenStack resources: network, subnet, router, gateway, interface, flavor, image, server, floating ip, security group, security group rules, volume | See lab exercises in Compendia Chp 1 |
| 3 | **Windows Server 2019 and PowerShell** ([Compendia Chp 2](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf), [GUI Windows Server](https://ntnu.blackboard.com/bbcswebdav/pid-1242343-dt-content-rid-33479523_1/xid-33479523_1), [PowerShell](./powershell.md))<br/>reboot, administrative tools (WinKey + X), task manager, (sconfig), search function, roles and features, Windows Admin Center, Networking: interface/ipaddress/netmask/gateway/DNS, updates, local users and groups, eventlog, Windows Defender, cmd, chocolatey, PowerShell: versions, ExecutionPolicy, cmdlet, parameter, alias, help, Get-Alias, Get-ChildItem, Get-Help, Get-Service, Select-String, drive, profile, variable, namespace, environment, Get-PSDrive, Get-Content, Write-Output, object/property/method, Resolve-DnsName, pipeline, Select-Object, Get-Process, Where-Object, $_, Format-List, Format-Table, Out-File, Export-Csv, ForEach-Object, Sort-Object, Get-Date, Measure-Object | See lab exercises in Compendia Chp 2 |
| 4 | **Windows Server 2019 and PowerShell** ([Compendia Chp 3](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf), [PowerShell](./powershell.md))<br/>Active Directory, scheduled tasks, PowerShell: modules, Find-Module, Install-Module, Get-WindowsUpdate, here-string, scripts, call operator, Invoke-ScriptAnalyzer, remoting, winrm, ssh, Enter-PSSession, New-PSSession, Get-PSSession, Invoke-Command, Get-NetTCPConnection, Test-WSMan, Get-SmbShare, New-SmbShare, Remove-SmbShare | **Portfolio 1 Release Monday**<br/>(Ransomware Protection) |
| 5  | **Data storage, backup and restore** |  |
| 6  | Lab week | **Portfolio 1 Hand-in Sunday** |
| 7  | **Logging and Monitoring** |  |
| 8  | **Directory Services: DNS** |  |
| 9  | **Directory Services: AD** | Portfolio 2: (Policy/Auth/Id?) |
| 10 | **Configuration Management: Group Policy** |  |
| 11 | **Package mgmt/Patching/Updates**  |  |
| 12 | **Risk and threats: Incentive, Capability, Vulnerability** | Portfolio 3 (Software?) |
| 13 | Påskeferie |  |
| 14 | **Windows Server Hardening and "Get the basics right"** |  |
| 15 |  |  |
| 16 |  |  |
| 17 |  | Portfolio 4 (Cloud?) |

<!--
(0. Hvordan dele inn i grupper? Mappeeval-oppgaver)
1. Heat stacks m disabled patch Tuesday i Win srv 2019?
LESE DE TO LEKSJONENE FRA Tor Ivar
2. Lab for å starte Heat stacks
3. To uker med PowerShell Menti og øvingsoppgaver
4. .

Start-Process powershell -ArgumentList "echo 5 M 15 | sconfig;sleep 10" -WindowStyle hidden

| Week | Learning outcome | Topic | Readings | Exercises/Portfolio |
| ---  | ---              | ---   | ---      | ---                 |
| 2 | K1,F1 | Cloud Computing (Horizon/OpenStack CLI) | [Cloud Computing](https://en.wikipedia.org/w/index.php?title=Cloud_computing&oldid=929942077), [OpenStack](https://en.wikipedia.org/w/index.php?title=OpenStack&oldid=932084017), Compendia Chp 1 | Do all "Lab tutorials" in Compendia Chp 1 (then do the "Review questions and Problems") |
| 3	| K1,K3,F1,F2 | Orchestration (OpenStack Heat) | [Complete idiot's introduction to yaml](https://github.com/Animosity/CraftIRC/wiki/Complete-idiot%27s-introduction-to-yaml), [An Introduction to OpenStack Heat](http://blog.scottlowe.org/2014/05/01/an-introduction-to-openstack-heat/), [Another Look at an OpenStack Heat Template](http://blog.scottlowe.org/2014/05/02/another-look-at-an-openstack-heat-template/), Compendia Chp 2 | Do all "Lab tutorials" in Compendia Chp 2 (then do the "Review questions and Problems") |
| 4	| F1,F2 | PowerShell | [PowerShell tutorial](https://gitlab.com/erikhje/dcsg1005/blob/master/powershell.md), Compendia Chp 3 | Do all "Lab tutorials" in Compendia Chp 3 (then do the "Review questions and Problems") |
| 5-6 | F1,F2,F5 | PowerShell environment | [PowerShell tutorial](https://gitlab.com/erikhje/dcsg1005/blob/master/powershell.md), Textbook Chp 1, 2, Compendia Chp 3 | Do all "Lab tutorials" in Compendia Chp 3 (then do the "Review questions and Problems") |
| 7-8 | F4 | DNS, AD (guest lecture AD w/Danny) | Textbook Chp 2, 3, [How DNS works](https://howdns.works/) (read all episodes incl bonus episode), Compendia Chp 4 | Frist Oblig1 (17.feb kl 16:00) Do all "Lab tutorials" in Compendia Chp 4 (then do the "Review questions and Problems") |
| 9 | K3,F4 | DNS, LDAP, Kerberos and AD, Group policy (guest lecture with Vebjørn 1015-1040) | [Group Policy](https://en.wikipedia.org/w/index.php?title=Group_Policy&oldid=942123014), Textbook Chp 3, Compendia Chp 4 | Frist Oblig2 (27.feb kl 16:00) Do all "Lab tutorials" in Compendia Chp 4 (then do the "Review questions and Problems") |
| 10 | K3,F4 | Storage and file services | Textbook Chp 4,5, Compendia Chp 5 | Do all "Lab tutorials" in Compendia Chp 5 (then do the "Review questions and Problems") |
| 11 | K3,F5 | Updates, Patching, Packages | Textbook Chp 6, Compendia Chp 6 | Do all "Lab tutorials" in Compendia Chp 6 (then do the "Review questions and Problems") |
| 12 | F2,F5 | Web Services and Certificates | Textbook Chp 9 | Do all "Lab tutorials" in Compendia Chp 7 (then do the "Review questions and Problems") |
| 13 | K2,F3,F5,F6  | Performance/Troubleshooting/Centralized logging/Monitoring | Textbook Chp 13,14 | Frist Oblig3 (25.mars kl 16:00) Do all "Lab tutorials" in Compendia Chp 8 (then do the "Review questions and Problems") | 
| 14 | | Security ([BlackHat Video](https://www.youtube.com/watch?v=FVe0Uaa65z0)) | [Active Directory Security Fundamentals](https://identityaccessdotmanagement.files.wordpress.com/2019/12/ad-security-fundamentals-1.pdf) | Do all "Lab tutorials" in Compendia Chp 9 (then do the "Review questions and Problems") |
| 16 | | Project | | |	 	 
| 17 | | Project | | | 
| 18 | | Project (+ Repetition, Exam info) | | Project deadline May 3rd | 
| 22 | | Digital home exam Inspera May 27th | | |
-->
