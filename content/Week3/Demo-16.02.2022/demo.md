---
order: 4
---

# Purpose 
This document entails all functionalities added for the "public-info" sprint demonstration.

&nbsp;  
&nbsp;  
## Creating websites/posts through markdown documents
Users can publish any markdown document as a website available to students in the "_posts" folder.
Each markdown document within are made into websites with its content converted into html.
Jekyll (Our site generator) needs each markdown document to contain the following section at its beginning:

```text
---
layout: nameOfLayout
title:  "nameOfWebsite"
date:   YYYY-MM-DD H:M:S -timezoneSpecific
categories: relationBetweenPosts
toc: true/false
---
```

## Dynamic Table of Content
To enable table of content for a given post the "toc" variable in the beginning section of a markdown document must be set to "true" and layout set to "post_toc".
Table of content is generated from all headings inside the given document.
For example:
```text
---
layout: post_toc
toc: true
---
```


In practice markdown headings are converted to html. Jekyll will afterwards group all html headings into a table of content:
```text
# heading1      ~   <h1>heading1</h1>
## heading2     ~   <h2>heading2</h2>
### heading3    ~   <h3>heading3</h3>
.....
```

&nbsp;  
&nbsp;  
## Dynamic linking to pdf/jpg/jpeg/pdf documents in same folder of a post
"_posts" folder is the top level folder for all posts.
Teachers are enabled to create their own folder structure beneath. 

For example:

```text
_posts                                              //Obligatory folder for jekyll (our site generator to find posts to become websites)
  -> Schedule                                       //user defined folder structure....
  -> Week1                                          //..
  -> Week2                                          //..
    -> 2022-02-10-first-teaching-material.md        //Markdown documents become posts/websites that entails the learning material
    -> 2022-02-10-second-teaching-material.md       //..
    -> nameOfImage.png                              //Image is linked with any markdown document in parent folder, namely Week2
    -> otherAttachedDocument.pdf                    //.., in fact it can be easily refferenced with [Link text](nameOfDocuement.extension) without the trailing file path
```
In addition, all files inside a given folder beneath _posts is available in the navigation menu of related websites.
Furthermore, when visiting 2022-02-10-first/second-material.md both posts/sites will have links in the navbar for all png/pdf/jpg/jpeg documents.
&nbsp;  
&nbsp;  
## Syntax highlighting for code snippets
[Rouge](https://github.com/rouge-ruby/rouge) is Jekyll's default syntax highlighter.

As of now we are mostly using Rouge's default syntax highlighting. Which serves well visually for most cases, but can be lacking with regards to what students are familiar with in the respective environments. For example powershell commandlets are usually yellow when using powershell and is not set in the default highlighting. It is notewhorty that we can in future improvements create own defined css for the most common used languages.

### Powershell

### Rouge

{% highlight powershell %}

Get-ChildItem | Select-Object -First 3

{% endhighlight %}

### Markdown

```powershell
Get-ChildItem | Select-Object -First 3
```

### C

### Rouge

{% highlight c %}
#include <stdio.h>

int main() {
    printf("Hello, World!");
    return 0;
}
{% endhighlight %}

### Markdown

```c
#include <stdio.h>

int main() {
    printf("Hello, World!");
    return 0;
}
```

### C++

{% highlight cpp %}

#include <iostream>
int main() {
    std::cout << "Hello, World!" << endl;
    return 0;
}
{% endhighlight %}

### Java

{% highlight java %}
class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello, World!"); 
    }
}
{% endhighlight %}

&nbsp;  
&nbsp;  
## 