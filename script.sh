#!/bin/sh

# Replicates the contents of the "content" directory in the webpage content directory.
# Automatically adds _posts directories to allow Jekyll to find markdown files and related assets.

# Remove any existing files and directories in ./webpage/content
[ -d "./webpage/content" ] && rm -rf ./webpage/content

# Retrieves a list of all directories within and including ./content
directories=($(find ./content -type d))

# Checks for _posts directories in ./content
ls -R ./content | grep --no-messages _posts >> /dev/null
if [ $? -eq 0 ]
then 
    echo "_posts folder found; structure unchanged."
    exit 0
else
    echo "Moving content file structure..."
fi

# Replicate file structure
for dir in ${directories[@]}
do
    # Recreate directory in webpage content
    mkdir ./webpage/$dir

    # Retrieves all non-directory files within the current directory (grep notes below)
    files=($(ls -l $dir | grep '^-' | grep -o '[^ ]*$'))

    # If file array not empty
    if ((${#files[@]}))
    then 
        # Create _posts folder in webpage directory
        mkdir ./webpage/$dir/_posts

        # Copy files to webpage directory
        for file in ${files[@]}
        do
            cp $dir/$file ./webpage/$dir/_posts/$file
        done
    fi
done

echo "Webpage structure complete!"

# Grep notes:
#   grep '^-':
#       Fetches only files (checks if first char of ls -l line is '-')
#   grep -o '[^ ]*$':
#       outputs only filename (looks for all non-whitespace characters adjacent to end of line)